"use strict";

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
const props = ["author", "name", "price"];
const fragment = document.createDocumentFragment();

class List {
    constructor(id, classes) {
        this.id = id;
        this.classes = classes;
    }

    render(listItems) {
        this.list = document.createElement("ul");
        this.list.id = this.id;
        this.list.classList.add(...this.classes);
        this.list.append(listItems);

        return this.list;
    }
}

class ListItem {
    constructor(classes) {
        this.classes = classes;
    }

    render(innerElement) {
        this.listItem = document.createElement("li");
        this.listItem.classList.add(...this.classes);
        this.listItem.append(innerElement);

        return this.listItem;
    }
}

class BookValidator {
    constructor(bookArray, properties, fragment) {
        this.bookArray = bookArray;
        this.properties = properties;
        this.fragment = fragment;
    }

    validate() {
        this.bookArray.forEach(book => {
            try {
                this.properties.forEach(prop => {
                    if (!Object.keys(book).includes(prop)) {
                        throw new Error(`In object: \n${JSON.stringify(book)} \nis missing property: ${prop}`);
                    } else {
                        if (prop === "price") {
                            const text = Object.entries(book).reduce((res, curr) => {
                                res += `${curr[0]} : ${curr[1]}, `;
                                return res;
                            }, "")

                            const li = new ListItem(["book"]).render(text.substring(0, text.length - 2));
                            this.fragment.append(li);
                        }
                    }
                })
            } catch (error) {
                console.log(error.message);
            }
        })
    }
}

const validatedBooks = new BookValidator(books, props, fragment);
validatedBooks.validate()
const root = document.querySelector("#root");
const title = document.createElement("h3");
title.innerText = "Books:";
const bookList = new List("books", ["bookList"]);

root.append(title, bookList.render(fragment));
